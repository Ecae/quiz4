__author__ = 'jhoeflich2017'


i = str(input('Enter the name for a text file you want to open '))
vowels = {'a', 'e', 'i', 'o', 'u'}
consonants = {'b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','y','z','B','C','D','F','G','H','J','K','L','M','N','P','Q','R','S','T','V','W','X','Y','Z'}

def find_vowels(txt):
    for vowel in vowels:
        if vowel in txt:
            print(vowel, txt.count(vowel))


def find_consonants(txt):
    for consonant in consonants:
        if consonant in txt:
            print(consonant, txt.count(consonant))



with open(i , 'r') as file:
    fileText = list(file.read())
find_vowels(fileText)
find_consonants(fileText)